import React, { Component } from 'react'
import ItemGlass from './ItemGlass'

export default class GlassList extends Component {
    renderListGlass = () => { 
        return this.props.glassArr.map((item) => {
            return (
                <ItemGlass
                    handleVirtualGlass = {this.props.handleVirtualGlass}
                    data={item} >
                </ItemGlass>
            )
        })
    }
  render() {
    return (
        <div>             
            <h2>Virtual Glasses</h2>
            <div class="row">
                {this.renderListGlass()}
            </div>
        </div>
    )
  }
}
